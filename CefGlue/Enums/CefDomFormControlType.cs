﻿namespace Xilium.CefGlue
{
    public enum CefDomFormControlType
    {
        Unsupported = 0,
        ButtonButton,
        ButtonSubmit,
        ButtonReset,
        ButtonSelectList,
        ButtonPopover,
        Fieldset,
        InputButton,
        InputCheckbox,
        InputColor,
        InputDate,
        InputDatetimeLocal,
        InputEmail,
        InputFile,
        InputHidden,
        InputImage,
        InputMonth,
        InputNumber,
        InputPassword,
        InputRadio,
        InputRange,
        InputReset,
        InputSearch,
        InputSubmit,
        InputTelephone,
        InputText,
        InputTime,
        InputUrl,
        InputWeek,
        Output,
        SelectOne,
        SelectMultiple,
        SelectList,
        TextArea,
    }
}
