﻿namespace Xilium.CefGlue
{
    public enum CefColorVariant
    {
        System,
        Light,
        Dark,
        TonalSpot,
        Neutral,
        Vibrant,
        Expressive
    }
}
