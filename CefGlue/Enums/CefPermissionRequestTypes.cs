﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_permission_request_types_t.
//
namespace Xilium.CefGlue
{
    using System;

    /// <summary>
    /// Permission types used with OnShowPermissionPrompt. Some types are
    /// platform-specific or only supported with the Chrome runtime. Should be kept
    /// in sync with Chromium's permissions::RequestType type.
    /// </summary>
    [Flags]
    public enum CefPermissionRequestTypes
    {
        None = 0,
        AccessibilityEvents = 1 << 0,
        ArSession = 1 << 1,
        CameraPanTiltZoom = 1 << 2,
        CameraStream = 1 << 3,
        CapturedSurfaceControl = 1 << 4,
        Clipboard = 1 << 5,
        TopLevelStorageAccess = 1 << 6,
        DiskQuota = 1 << 7,
        LocalFonts = 1 << 8,
        Geolocation = 1 << 9,
        HandTracking = 1 << 10,
        IdentityProvider = 1 << 11,
        IdleDetection = 1 << 12,
        MicStream = 1 << 13,
        MidiSysEx = 1 << 14,
        MultipleDownloads = 1 << 15,
        Notifications = 1 << 16,
        KeyboardLock = 1 << 17,
        PointerLock = 1 << 18,
        ProtectedMediaIdentifier = 1 << 19,
        RegisterProtocolHandler = 1 << 20,
        StorageAccess = 1 << 21,
        VrSession = 1 << 22,
        WebAppInstallation = 1 << 23,
        WindowManagement = 1 << 24,
        FileSystemAccess = 1 << 25,
    }
}
