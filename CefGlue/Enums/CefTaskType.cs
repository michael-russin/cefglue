﻿namespace Xilium.CefGlue
{
    public enum CefTaskType
    {
        Unknown = 0,
        /// The main browser process.
        Browser,
        /// A graphics process.
        GPU,
        /// A Linux zygote process.
        Zygote,
        /// A browser utility process.
        Utility,
        /// A normal WebContents renderer process.
        Renderer,
        /// An extension or app process.
        Extension,
        /// A browser plugin guest process.
        Guest,
        /// A plugin process.
        Plugin,
        /// A sandbox helper process
        SandboxHelper,
        /// A dedicated worker running on the renderer process.
        DedicatedWorker,
        /// A shared worker running on the renderer process.
        SharedWorker,
        /// A service worker running on the renderer process.
        ServiceWorker,
    }
}
