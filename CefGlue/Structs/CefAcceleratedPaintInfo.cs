﻿using System;
using Xilium.CefGlue.Interop;

namespace Xilium.CefGlue
{
    public unsafe ref struct CefAcceleratedPaintInfo
    {
        private readonly cef_accelerated_paint_info_t* _target;

        internal CefAcceleratedPaintInfo(cef_accelerated_paint_info_t* target)
        {
            _target = target;
        }

        /// Handle for the shared texture. The shared texture is instantiated
        /// without a keyed mutex.
        ///
        UIntPtr shared_texture_handle;

        public IntPtr SharedTextureHandle
        {
            get
            {
                CheckSelf();
                return _target->shared_texture_handle;
            }
        }

        ///
        /// The pixel format of the texture.
        ///
        public CefColorType Format
        {
            get
            {
                CheckSelf();
                return _target->format;
            }
        }

        private readonly void CheckSelf()
        {
            if (_target == null) ThrowCheckSelfFailed();
        }

        private static void ThrowCheckSelfFailed()
        {
            throw new InvalidOperationException("CefAcceleratedPaintInfo is null.");
        }
    }
}
