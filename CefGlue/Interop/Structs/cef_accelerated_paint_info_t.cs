﻿//
// This file manually written from cef/include/internal/cef_types.h.
//

using System;
using System.Runtime.InteropServices;

namespace Xilium.CefGlue.Interop
{
    [StructLayout(LayoutKind.Sequential, Pack = libcef.ALIGN)]
    internal unsafe struct cef_accelerated_paint_info_t
    {
        /// Handle for the shared texture. The shared texture is instantiated
        /// without a keyed mutex.
        ///
        public IntPtr shared_texture_handle;

        ///
        /// The pixel format of the texture.
        ///
        public CefColorType format;

        #region Alloc & Free
        private static int _sizeof;

        static cef_accelerated_paint_info_t()
        {
            _sizeof = Marshal.SizeOf(typeof(cef_accelerated_paint_info_t));
        }

        public static cef_accelerated_paint_info_t* Alloc()
        {
            var ptr = (cef_accelerated_paint_info_t*)Marshal.AllocHGlobal(_sizeof);
            *ptr = new cef_accelerated_paint_info_t();
            return ptr;
        }

        public static void Free(cef_settings_t* ptr)
        {
            Marshal.FreeHGlobal((IntPtr)ptr);
        }
        #endregion
    }
}
